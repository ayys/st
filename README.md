## My fork of ST - Simple Terminal

st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.

Download
--------

[st - Simple Terminal download link for x64 Linux systems](http://gitlab.com/ayys/st/builds/artifacts/master/download?job=build "Simple Terminal Download") - st is compiled via Gitlab CI/CD. Download the zip file, extract ```st``` and put it in your ```$PATH```. Then you can run it yourself. ALthough, I would recomend compiling it yourself.

Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.
