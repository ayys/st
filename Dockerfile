FROM ubuntu

RUN apt-get update &&\
	apt-get install -y make\
    	pkg-config\
    	\
    	libx11-dev\
    	gcc\
    	git\
    	libfontconfig1-dev\
    	fontconfig\
    	libxft-dev
